#include <stdio.h>

class RS232
{
public:
    
    RS232(const char * pathname, int flags);
    size_t writeFile(const char * msg, size_t size);
    size_t readFile();
    int closeFile();
    
    
private:
    
    int fd; // Serial port
    
};
