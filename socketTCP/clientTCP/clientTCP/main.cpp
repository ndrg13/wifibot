#include <cstdlib>
#include <iostream>
#include <string>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <stdio.h>
#include "socket.hpp"

int main(int argc, const char * argv[])
{
    Socket clntSock("clnt", AF_INET, SOCK_STREAM, 0);
    clntSock.createSin("serv", AF_INET, 15020, INADDR_LOOPBACK);
    
    std::cout << "Connexion au server..." << std::endl;
    int connect_err = clntSock.connectSocket();
    
    if (connect_err != -1)
    {
        std::string msg;
        std::cout << "Client connecté ! Entrer un message à transmettre :" << std::endl;
        std::cin >> msg;
        int send_err = clntSock.sendSocket("clnt", msg);
        
        if (send_err != -1)
        {
            std::cout << "Message transmis !" << std::endl;
        }
        else
        {
            std::cout << "Erreur de transmission" << std::endl;
            clntSock.closeSocket("clnt");
        }
    }
    else
    {
        std::cout << "Erreur lors de la connexion" << std::endl;
        clntSock.closeSocket("clnt");
    }
    
    return 0;
}
