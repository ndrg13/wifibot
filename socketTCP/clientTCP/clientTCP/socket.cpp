#include <unistd.h>
#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <string>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include "socket.hpp"

#define INVALID_SOCKET -1
#define SOCKET_ERROR -1

Socket::Socket(std::string host, int domain, int type, int protocol) // Création du socket
{
    if (host == "serv")
        servSock = socket(domain, type, protocol);
    else if (host == "clnt")
        clntSock = socket(domain, type, protocol);
};

int Socket::getSock(std::string host)
{
    int temp;
    if (host == "serv")
        temp = servSock;
    else if (host == "clnt")
        temp = clntSock;
    else
        temp = -1;
    
    return temp;
}

void Socket::createSin(std::string host, sa_family_t sinFamily, in_port_t sinPort, const char *sinAddress) // Paramétrage du socket (contexte d'adressage)
{
    if (host == "serv")
    {
        sin.sin_family = sinFamily;
        sin.sin_port = htons(sinPort);
        sin.sin_addr.s_addr = inet_addr(sinAddress);
    }
    else if (host == "clnt")
    {
        clntSin.sin_family = sinFamily;
        clntSin.sin_port = htons(sinPort);
        clntSin.sin_addr.s_addr = inet_addr(sinAddress);
    }
};

void Socket::createSin(std::string host, sa_family_t sinFamily, in_port_t sinPort, in_addr_t sinAddress) // Paramétrage du socket (contexte d'adressage)
{
    if (host == "serv")
    {
        sin.sin_family = sinFamily;
        sin.sin_port = htons(sinPort);
        sin.sin_addr.s_addr = htonl(sinAddress);
    }
    else if (host == "clnt")
    {
        clntSin.sin_family = sinFamily;
        clntSin.sin_port = htons(sinPort);
        clntSin.sin_addr.s_addr = htonl(sinAddress);
    }
};

in_port_t Socket::getPort(std::string host)
{
    in_port_t temp;
    if (host == "serv")
        temp = sin.sin_port;
    else if (host == "clnt")
        temp = clntSin.sin_port;
    else
        temp = -1;
    
    return temp;
};

int Socket::bindSocket(std::string host) // Association du soket au contexte d'adressage
{
    int temp;
    if (host == "serv")
        temp = bind(servSock, (struct sockaddr *)&sin, sizeof(sin));
    else if (host == "clnt")
        temp = bind(clntSock, (struct sockaddr *)&clntSin, sizeof(clntSin));
    else
        temp = -1;
    
    return temp;
};

int Socket::listenSocket(int logMax)
{
    int listen_err = listen(servSock, logMax);
    return listen_err;
};

int Socket::connectSocket()
{
    int connect_err = connect(clntSock, (struct sockaddr *)&sin, sizeof(sin));
    return connect_err;
};

void Socket::acceptSocket()
{
    socklen_t clntSinSize = sizeof(clntSin);
    clntSock = accept(servSock, (struct sockaddr *)&clntSin, &clntSinSize);
};

int Socket::sendSocket(std::string host, std::string msg)
{
    int temp;
    const char *strToSend = msg.c_str();
    if (host == "serv")
        temp = send(servSock, strToSend, sizeof(strToSend), 0);
    else if (host == "clnt")
        temp = send(clntSock, strToSend, sizeof(strToSend), 0);
    else
        temp = -1;
    
    return temp;
};

int Socket::recvSocket(std::string host)
{
    int temp;
    if (host == "serv")
        temp = recv(servSock, strRecv, sizeof(strRecv), 0);
    else if (host == "clnt")
        temp = recv(clntSock, strRecv, sizeof(strRecv), 0);
    else
        temp = -1;
    
    return temp;
};

char* Socket::getStrRecv()
{
    return strRecv;
};

int Socket::closeSocket(std::string host)
{
    int temp;
    if (host == "serv")
        temp = close(servSock);
    else if (host == "clnt")
        temp = close(clntSock);
    else
        temp = -1;
    return temp;
};
