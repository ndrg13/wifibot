#include <unistd.h>
#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <string>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <arpa/inet.h>

class Socket //
{
public:
    
    Socket(std::string host, int domain, int type, int protocol);
    int getSock(std::string host);
    void createSin(std::string host, sa_family_t sinFamily, in_port_t sinPort, const char *sinAddress);
    void createSin(std::string host, sa_family_t sinFamily, in_port_t sinPort, in_addr_t sinAddress);
    in_port_t getPort(std::string host);
    int bindSocket(std::string host);
    int listenSocket(int logMax);
    int connectSocket();
    void acceptSocket();
    int sendSocket(std::string host, std::string msg);
    int recvSocket(std::string host);
    char* getStrRecv();
    int shutdownSocket();
    int closeSocket(std::string host);
    
private:
    
    int servSock, clntSock;
    struct sockaddr_in sin, clntSin;
    char *strRecv;
};

