#include <unistd.h>
#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <string>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include "socket.hpp"

int main(int argc, const char * argv[])
{
    Socket servSock("serv", AF_INET, SOCK_STREAM, 0);
    servSock.createSin("serv", AF_INET, 15020, INADDR_ANY);
    int bind_err = servSock.bindSocket("serv");
    
    if (bind_err != -1)
    {
        std::cout << "Le socket est maintenant configuré" << std::endl;
        int listen_err = servSock.listenSocket(5);
        
        if (listen_err != -1)
        {
            std::cout << "Le port " << servSock.getPort("serv") << " est maintenant sur écoute... En attente d'un client" << std::endl;
            servSock.acceptSocket();
            
            if (servSock.getSock("clnt") != -1)
            {
                std::cout << "Client connecté ! En attente de réception..." << std::endl;
                int recv_err = servSock.recvSocket("clnt");
                char *strToDisplay = servSock.getStrRecv();
                std::cout << "Message reçu : " << puts(strToDisplay) << "\n" << "Fin de transmission" << std::endl;
                servSock.closeSocket("clnt");
                
                if (recv_err != -1)
                    std::cout << "Message transmis" << std::endl;
                else
                    std::cout << "Erreur de transmission" << std::endl;
            }
            else
            {
                std::cout << "Erreur lors de la connexion du client" << std::endl << errno << std::endl;
            }
        }
        else
        {
            std::cout << "Erreur lors de la mise sur écoute du port" << std::endl << errno << std::endl;
        }
    }
    else
    {
        std::cout << "Erreur lors de l'association socket / contexte d'adressage" << std::endl << errno << std::endl;
    }
    
    return 0;
}
